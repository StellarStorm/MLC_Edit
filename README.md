# MLC Edit
MLC Edit is designed to take a standard RP dicom file and add motion, or
errors, to individual leaf jaw positions in the MLC device. The new positions
can be defined in the following ways:  
(a). Loading a "parent" RP dicom and either defining a uniform distribution from
    which random motion will be generated (if the "Randomize" box is ticked),
    or defining uniform motion for all leaves. In both cases, the motion is
    then individually added to each leaf in the MLC.  
(b). Loading a "parent" dicom and then extracting the Leaf Jaw Positions into
    a .xlsx file. The positions in this file can then be edited by hand, with
    a VBA script, and so on.  
After the modifications are complete, the new RP dicom is written. Please note
that only the MLC leaf jaw positions, the RT Plan Label, and (optionally) the
SOP Instance UID can be modified by this program. All other attributes are
determined by the parent RP dicom.

![MLC Edit](images/mainwindow.png)

## Prerequisites
[Python](https://www.python.org/) >= 3.4  
[pydicom](https://github.com/pydicom/pydicom) >= 1.0.0  
[Shapely](https://github.com/Toblerity/Shapely)  
[Numpy](http://www.numpy.org/)  
[PyQt5](https://pypi.python.org/pypi/PyQt5)  
[Pandas](https://pandas.pydata.org)  
[Xlsxwriter](https://xlsxwriter.readthedocs.io/)

For Windows and macOS, we recommend using the
[Anaconda python distribution](https://www.anaconda.com/download/).  
Linux users should be fine with whichever Python 3 version that came with their
distribution. When possible, use your package manager to install dependencies
instead of `pip`.


## Known limitations:
(1). When plan has two MLC layers (upper and lower):
1. "Random" shifts of 0. When choosing to randomize the leaf shifting but using
0 as the random distribution, some leaves will still be shifted. This occurs
because the randomization process also checks for overlaps and potential leaf
conflicts. When a conflict is detected, it moves the leaf or leaves involved to
a safe position.  
If you want to avoid this behavior, simply uncheck the "Randomize" box.

A more complex version of this occurs with randomizations of greater than 0.

# License
Source code is copyright (C) 2017-2018 S. Gay, and provided under the MIT
license. See LICENSE.md for details.  
The 'hospital.png' icon is provided by Google's Material Design icon set and
published under the Apache 2.0 license. See BundledLicenses.md for details.

Please note that provided binaries packages may contain libraries or
other code portions that are published under different licenses, such as PyQt5.
See BundledLicenses.md for details.
