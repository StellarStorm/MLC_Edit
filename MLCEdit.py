# -*- coding: utf-8 -*-
"""
MLCEdit.py is designed to take a standard RP dicom file and add motion, or
errors, to individual leaf jaw positions in the MLC device. The new positions
can be defined in the following ways:
(a). Loading a "parent" RP dicom and either defining a uniform distribution from
    which random motion will be generated (if the "Randomize" box is ticked),
    or defining uniform motion for all leaves. In both cases, the motion is
    then individually added to each leaf in the MLC.
(b). Loading a "parent" dicom and then extracting the Leaf Jaw Positions into
    a .xlsx file. The positions in this file can then be edited as wanted.
After the modifications are complete, the new RP dicom is written. Please note
that only the MLC leaf jaw positions, the RT Plan Label, and (optionally) the
SOP Instance UID can be modified by this program. All other attributes are
determined by the parent RP dicom.
"""

import csv
import ctypes
from datetime import datetime
from copy import deepcopy
from getpass import getuser
import logging
import os
from platform import node, platform, system
import re
from subprocess import check_output
import sys
import numpy as np
import pandas as pd
import pydicom
from PyQt5 import QtWidgets, QtGui, QtCore
from shapely import affinity
from shapely.geometry import box, LineString
# Local modules
from MLCEui import Ui_MainWindow
from notifiCat import errorNotif, infoNotif, askQuestion

import twoLayer.utils as utils
from twoLayer.newPositions import Dependent as dep
from twoLayer.corrections import DependentCorrections as dc


class Gui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.listWidget_CP.clicked.connect(self.cpInfo)
        self.ui.pushButton_loadDicom.clicked.connect(self.getDicom)
        self.ui.pushButton_Reset.clicked.connect(self.reset)
        self.ui.pushButton_clearTmp.clicked.connect(self.clearTmp)
        self.ui.pushButton_Go.clicked.connect(self.moveMLC)
        self.ui.pushButton_extractLJP.clicked.connect(self.extractLJP)
        self.ui.pushButton_externPlan.clicked.connect(self.externPlan)
        self.ui.radioButton_oneLayer.clicked.connect(self.layerNum)
        self.ui.radioButton_twoLayers.clicked.connect(self.layerNum)
        self.ui.radioButton_geom28.clicked.connect(self.getGeom)
        self.ui.radioButton_geom60.clicked.connect(self.getGeom)
        self.ui.radioButton_geomCustom.clicked.connect(self.getGeom)

        self.ui.actionQuit.triggered.connect(QtWidgets.qApp.quit)
        self.ui.actionAbout.triggered.connect(self.about)
        self.ui.actionAbout_Qt.triggered.connect(self.aboutQt)
        self.ui.actionOpen_Spreadsheet.triggered.connect(self.openOutput)

        self.ui.lineEdit_geomCustom.setEnabled(False)
        self.ui.lineEdit_uniformDist.textChanged.connect(self.enableGo)

        self.ui.label_rtPlanLabel.setEnabled(False)
        self.ui.lineEdit_rtPlanLabel.setEnabled(False)

        self.ui.pushButton_Go.setEnabled(False)
        self.ui.pushButton_extractLJP.setEnabled(False)

        self._process = QtCore.QProcess(self)
        self.externFile = ''
        self.filename = ''
        self.header = ''
        self.mlcType = 'oneLayer'
        self.originalCol = ''
        self.colAngles = 0
        self.layers = 0
        self.minSep = 0.52
        self.geomX = 140.0
        self.geomY = 140.0
        self.ud = 0
        self.beams = 0
        # If the user has cloned the repo with git and is running the source
        # code, use the latest tag + checksum as version
        try:
            git_sum = check_output(['git', 'describe', '--always'])
            self.version = git_sum.decode()
        except Exception:
            self.version = 'v1.0.0-alpha1'
        self.leafBoundaries = []
        self.newljp = []
        self.oldljp = []
        self.tmpljp = []
        self.upperStatVals = []
        self.lowerStatVals = []
        self.statVals = []
        self.planInfo = {}
        self.pwd = os.getcwd()

        # Make sure necessary directories exists
        os.makedirs(os.path.join(self.pwd, 'Output'), exist_ok=True)
        os.makedirs(os.path.join(self.pwd, 'Logs'), exist_ok=True)

        self.handleLogs(500)

    def about(self):
        """Brief description of the program."""

        sourceUrl = 'href="https://gitlab.com/MDA-Courtyard/MLC_Edit/blob/master/LICENSE.md"'
        pyUrl = 'href="https://www.python.org/"'
        pydicomUrl = 'href="https://github.com/pydicom/pydicom"'
        shapelyUrl = 'href="https://github.com/Toblerity/Shapely"'
        numpyUrl = 'href="http://www.numpy.org/"'
        pyqt5Url = 'href="https://pypi.python.org/pypi/PyQt5"'
        pandasUrl = 'href="https://pandas.pydata.org"'
        xlsxUrl = 'href="https://github.com/jmcnamara/XlsxWriter"'
        mdiUrl = 'href="https://material.io/icons/"'
        bundledUrl = 'href="https://gitlab.com/MDA-Courtyard/MLC_Edit/blob/master/BundledLicenses.md"'
        title = 'About'
        msg = (
            '<p><br><b>MLC Edit ' + self.version + '</b></br>'
            '<br>Easily add motion to MLC leaf positions for error studies.'
            '</br></p>'
            '<p>Copyright (C) 2017-2018 S. Gay under the <a %s>MIT license</a>.'
            '</p>'
            "<p>We are grateful for the following open source projects,"
            "without which MLC Edit could not exist:"
            "<br><a %s>Python</a></br>"
            "<br><a %s>Pydicom</a></br>"
            "<br><a %s>Shapely</a></br>"
            "<br><a %s>Numpy</a></br>"
            "<br><a %s>PyQt5</a></br>"
            "<br><a %s>Pandas</a></br>"
            "<br><a %s>Xlsxwriter</a></br>"
            "<br>Icon provided by Google's <a %s>Material Design</a> set.</br>"
            '</p>'
            '<p>All projects remain the property of their respective owners.'
            'See <a %s>Bundled Licenses</a> for more details.' %
            (sourceUrl, pyUrl, pydicomUrl, shapelyUrl, numpyUrl, pyqt5Url,
             pandasUrl, xlsxUrl, mdiUrl, bundledUrl))
        QtWidgets.QMessageBox.about(self, title, msg)

    def aboutQt(self):
        """Show a message box about Qt."""
        QtWidgets.QMessageBox.aboutQt(self)

    def handleLogs(self, maxCount):
        """
        Handle basic log setup and maintenance, like creating new logs whenever
        the current log gets beyond a certain line count (to prevent the log
        from growing so large it's difficult to open or read).

        :param maxCount: Maximum number of lines in file
        """
        logFile = os.path.join('Logs', 'mlceLog.log')

        try:
            with open(logFile, 'r') as f:
                contents = f.readlines()
                lines = sum(1 for line in contents)
                if lines > maxCount:
                    dates = []
                    for i, line in enumerate(contents):
                        if '=' in line:
                            dates.append(contents[i + 1])
                    startDate = dates[0].split(' ')[0]
                    endDate = dates[-1].split(' ')[0]
                    # We have to explicitly close the file so that it can be
                    # renamed
                    f.close()

                    # Rename the file. If a file with the same name already
                    # exists (this could occur if the program was used so much
                    # that the start and end dates are the same day), then
                    # incorporate the start and end time into the new file name
                    baseName = 'mlceLog' + '_' + startDate + '_' + endDate
                    name = os.path.join('Logs', baseName + '.log')

                    if os.path.isfile(name):
                        startDate = dates[0].replace(' ', '_').replace(
                            ':', '.')
                        startDate = startDate.strip()
                        endDate = dates[-1].replace(' ', '_').replace(':', '.')
                        endDate = endDate.strip()
                        baseName = 'mlceLog' + '_' + startDate + '_' + endDate
                        name = os.path.join('Logs', baseName + '.log')

                    os.rename(logFile, name)
                    msg = ('Log file has reached maximum size and was renamed '
                           'to %s. \nNew information will now be logged in  '
                           'mlceLog.log' % baseName)
                    infoNotif(self, 'Log file renamed', msg)

        except FileNotFoundError:
            pass

        logging.basicConfig(
            format='%(message)s', filename=logFile, level=logging.DEBUG)
        logging.info('\n==================================================')
        logging.info(datetime.now().strftime('%Y%m%d %H:%M:%S'))
        logging.info(system())
        logging.info(getuser())
        logging.info(node())
        logging.info(self.version.strip())

    def openOutput(self):
        """Open the spreadsheet to which ljp data is extracted."""
        location = os.path.join(self.pwd, 'Output')
        ssheet = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File',
                                                       location)
        ssheet = str(ssheet[0])
        if ssheet != '':
            print('Opening %r' % ssheet)
            if system() == 'Windows':
                # We have to run the command through a shell in Windows
                self._process.start('cmd.exe', ['/c', 'start', ssheet])
            elif system() == 'Darwin':
                self._process.start('open', [ssheet])
            elif system() == 'Linux':
                self._process.start('xdg-open', [ssheet])

    def reset(self):
        """Reset everything to first-start state."""
        self.ui.listWidget_CP.clear()
        self.ui.listWidget_planInfo.clear()
        self.ui.lineEdit_uniformDist.clear()
        self.ui.lineEdit_rtPlanLabel.clear()
        self.ui.radioButton_VMAT.setChecked(True)
        self.ui.pushButton_Go.setEnabled(False)
        self.ui.pushButton_extractLJP.setEnabled(False)
        self.ui.radioButton_Rand.setChecked(True)
        self.ui.checkBox_dryRun.setChecked(False)
        self.ui.label_rtPlanLabel.setEnabled(False)
        self.ui.lineEdit_rtPlanLabel.setEnabled(False)
        self.ui.lineEdit_geomCustom.setEnabled(False)
        self.setWindowTitle('MLC Edit')
        self.externFile = ''
        self.filename = ''
        self.header = ''
        self.ud = 0
        self.beams = 0
        self.layers = 0
        self.colAngles = []
        self.leafBoundaries = []
        self.newljp = []
        self.oldljp = []
        self.tmpljp = []
        self.upperStatVals = []
        self.lowerStatVals = []
        self.statVals = []
        self.planInfo = {}
        self.pwd = os.getcwd()
        msg = 'All variables cleared.'
        infoNotif(self, 'Cleared', msg)
        logging.info('\n\nself.reset() called')

    def clearTmp(self):
        """
        Only reset selected values - good when running multiple times on the
        same RP*.dcm so it doesn't have to be opened every time.
        """
        self.ui.listWidget_CP.clear()
        self.colAngles = []
        self.leafBoundaries = []
        self.newljp = []
        self.oldljp = []
        self.tmpljp = []
        self.upperStatVals = []
        self.lowerStatVals = []
        self.statVals = []
        self.planInfo = {}
        self.pwd = os.getcwd()
        msg = 'Temporary values cleared.'
        infoNotif(self, 'Cleared', msg)
        logging.info('\n\nTemp variables cleared.')
        try:
            self.findMLC()
        except AttributeError:  # Nothing has been loaded yet
            pass

    def enableExtractLJP(self):
        """Enables the Extract MLC button."""
        if self.filename != '':
            self.ui.pushButton_extractLJP.setEnabled(True)

    def enableGo(self):
        """Enables the "Go" button for random motion generation."""
        if (self.ud != 0 or self.ud != '') and (self.filename != ''):
            self.ui.pushButton_Go.setEnabled(True)
        else:
            return 1

    def layerNum(self):
        """Set the number of MLC layers"""
        if self.ui.radioButton_oneLayer.isChecked():
            self.layers = 1
            self.ui.checkBox_preserveLayers.setChecked(False)
            self.ui.checkBox_preserveLayers.setEnabled(False)
        elif self.ui.radioButton_twoLayers.isChecked():
            self.layers = 2
            self.ui.checkBox_preserveLayers.setEnabled(True)
            self.ui.checkBox_preserveLayers.setChecked(True)

    def getDicom(self):
        """Get the parent DICOM file."""
        self.filename = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Select DICOM')
        self.filename = str(self.filename[0])
        shortName = self.filename.split('/')[-1]

        if not pydicom.misc.is_dicom(self.filename):
            msg = '%s is not a valid DICOM image!' % shortName
            errorNotif(self, msg)
            self.filename = ''
            return

        print('RP:', self.filename)

        self.header = pydicom.read_file(self.filename)

        # Change the title to show the currently loaded RP*.dcm
        title = 'MLC Edit - ' + shortName
        self.setWindowTitle(title)

        # Display the RT Plan Label. This can be edited and will be
        # used to determine the name of the output RP*.dcm
        self.ui.lineEdit_rtPlanLabel.setText(self.header.RTPlanLabel)

        self.ui.label_rtPlanLabel.setEnabled(True)
        self.ui.lineEdit_rtPlanLabel.setEnabled(True)
        self.enableGo()
        self.enableExtractLJP()

        # Display info:
        self.ui.listWidget_planInfo.addItem(self.filename)
        self.ui.listWidget_planInfo.addItem(
            'Modality: %s' % self.header.Modality)
        self.ui.listWidget_planInfo.addItem(
            'Operator Name: %s' % str(self.header.OperatorsName))
        self.ui.listWidget_planInfo.addItem(
            'Patient Name: %s ' % str(self.header.PatientName))
        self.ui.listWidget_planInfo.addItem(
            'Patient ID: %s ' % self.header.PatientID)
        self.ui.listWidget_planInfo.addItem(
            'SOP Instance UID: %s ' % self.header.SOPInstanceUID)
        self.ui.listWidget_planInfo.addItem(
            'Instance Creation Time: %s' % self.header.InstanceCreationTime)
        self.ui.listWidget_planInfo.addItem(
            'RT Plan Label: %s ' % self.header.RTPlanLabel)
        self.ui.listWidget_planInfo.addItem(
            'Manufacturer: %s ' % self.header.Manufacturer)
        self.ui.listWidget_planInfo.addItem(
            'Model : %s ' % self.header.ManufacturerModelName)
        self.ui.listWidget_planInfo.addItem(
            'Approval Status: %s ' % self.header.ApprovalStatus)

        logging.info('\nRP.*dcm: %s ', self.filename)
        logging.info('Original UID: %s', self.header.SOPInstanceUID)
        logging.info('Original RT Plan Label: %s', self.header.RTPlanLabel)

        self.findMLC()

    def findMLC(self):
        """Find all MLC positions"""
        # Reset in case of previous runs
        self.oldljp = []
        self.colAngles = []
        self.ui.listWidget_CP.clear()

        # Get the number of arcs or fields in the plan - this will include the
        # conebeam imaging portion (if it exists):
        self.beams = len(self.header.BeamSequence)

        # When this loop finishes, we should have a Python nested list.
        # Each sublist should have the index of a single MLC data set, and
        # the position of every MLC leaf per control point within that set.
        oldljp = []
        headers = []
        for i, arc in enumerate(self.header.BeamSequence):
            for j, controlP in enumerate(arc.ControlPointSequence):
                # Halcyon doesn't have beamLimDevSeq for every control point
                # sequence like Truebeam does
                if 'BeamLimitingDevicePositionSequence' in controlP:
                    for k, positionSeq in enumerate(
                            controlP.BeamLimitingDevicePositionSequence):
                        if 'MLCX' in positionSeq.RTBeamLimitingDeviceType:
                            # This will display the Beam Limiting Devices
                            # Position Sequences that contain the MLC positions.
                            beamLimDevSeq = 'BeamSequence%s.ControlPointSequence%s.BeamLimitingDevicePositionSequence%s' % (
                                i, j, k)
                            self.ui.listWidget_CP.addItem(beamLimDevSeq)
                            # Get the 'header' - the
                            # Beam:ControlPoint:DevicePosition sequences - for
                            # each point
                            headers.append('%i:%i:%i' % (i, j, k))
                            # Leaf Jaw Positions
                            oldljp.append(list(positionSeq.LeafJawPositions))

            # Get the leaf position boundaries (defines leaf width in the
            # y-direction) for each leaf in an arc. This will remain the same
            # for all leaves in a particular layer, in a particular arc, but
            # may change across arcs.
            limitHold = []
            for devSeq in arc.BeamLimitingDeviceSequence:
                if 'MLCX' in devSeq.RTBeamLimitingDeviceType:
                    limitHold.append(list(devSeq.LeafPositionBoundaries))
            self.leafBoundaries.append(limitHold)

            angle = float(arc.ControlPointSequence[0].BeamLimitingDeviceAngle)
            self.colAngles.append(angle)

        # Convert all boundary values to float
        for i, arc in enumerate(self.leafBoundaries):
            for j, layer in enumerate(arc):
                for k, leaf in enumerate(layer):
                    self.leafBoundaries[i][j][k] = float(leaf)
        self.ui.listWidget_planInfo.addItem(
            'Number of fields (includes CBCT): '
            '%i' % self.beams)

        # Now convert original leaf positions to pandas dataframe. Each column
        # contains the leaf positions at a certain index
        #
        # First, add np.nan as needed to ensure all columns are same length.
        # This will be needed for machines like Halcyon that have two-layer MLCs
        # where one layer has fewer leaves than the other. I'm assuming the
        # length of each layer in the first control point will be the same for
        # all control points.
        list1len = len(oldljp[0])
        list2len = len(oldljp[1])

        # Single-MLC-layer machines
        if list1len == list2len:
            self.mlcType = 'oneLayer'
        # Halcyon
        elif list1len < list2len:
            self.mlcType = 'twoLayerA'
            diff = list2len - list1len
            for seq in oldljp[0::2]:
                seq.extend([np.nan] * diff)
            self.planInfo['dX1'] = range(0, int(list1len / 2))
            self.planInfo['dX2'] = range(int((list1len / 2)), list1len)
            self.planInfo['pX1'] = range(0, int(list2len / 2))
            self.planInfo['pX2'] = range(int((list2len / 2)), list2len)

        # Not aware of any machine like this
        elif list1len > list2len:
            self.mlcType = 'twoLayerB'
            diff = list1len - list2len
            for seq in oldljp[1::2]:
                seq.extend([np.nan] * diff)

        # We have to transpose here, so column headers will be the indices
        # for each leaf position sequence, and each position sequence
        # will be in a column, not a row.
        self.oldljp = pd.DataFrame(oldljp, index=headers, dtype=float).T

    def cpInfo(self):
        """
        Display the RT Beam Limiting Device Type (mlcx*) and leaf/jaw
        positions for all MLC control points, by selecting them from the GUI.
        """
        controlPoint = self.ui.listWidget_CP.currentItem().text()
        i, j, k = list(map(int, re.findall(r'\d+', controlPoint)))
        bLDevType = self.header.BeamSequence[i].ControlPointSequence[
            j].BeamLimitingDevicePositionSequence[k].RTBeamLimitingDeviceType
        bLDevType = 'RT Beam Limiting Device Type: %s' % bLDevType
        header = '%i:%i:%i' % (i, j, k)
        positions = self.oldljp.loc[:, header].values
        # Remove any np.nan's that were added to the upper layer (if
        # applicable)
        positions = positions[~np.isnan(positions)]
        x1 = 'Side X1: ' + str(np.split(positions, 2)[0])
        x2 = 'Side X2: ' + str(np.split(positions, 2)[1])
        position = (
            'Beam Sequence: %i\nControl Point Sequence: %i\nBeam Limiting Device Position Sequence: %i'
            % (i, j, k))
        infoNotif(self, 'Leaf Positions',
                  position + '\n\n' + bLDevType + '\n\n' + x1 + '\n\n' + x2)

    def extractLJP(self):
        """Extract MLC positions to XLSX file"""
        # We can later directly edit the ljp position in the spreadsheet and
        # create a new RP dicom from the spreadsheet.
        out_path = os.path.join(self.pwd, 'Output')
        # Set the output spreadsheet's name to match the RP dicom name.
        out_file = os.path.join(
            out_path,
            os.path.basename(self.filename).split('.')[0] + '.xlsx')

        writer = pd.ExcelWriter(out_file, engine='xlsxwriter')

        # Get info about file and run time variables
        valTypes = [
            'File Name', 'Run time', 'Layout', 'Patient Name', 'Patient ID',
            'Instance Creation Time', 'SOP Instance UID', 'RT Plan Label',
            'Manufacturer', 'Manufacturer Model Name', 'Approval Status',
            'RT Plan Name', 'Uniform Distribution (mm)',
            'TPS Software Version', 'MLC Edit version'
        ]
        vals = [
            self.filename,
            str(datetime.now()),
            'BeamSequence:ControlPointSequence:BeamLimitingDevicePositionSequence',
            str(self.header.PatientName), self.header.PatientID,
            self.header.InstanceCreationDate + ' ' +
            self.header.InstanceCreationTime, self.header.SOPInstanceUID,
            self.header.RTPlanLabel, self.header.Manufacturer,
            self.header.ManufacturerModelName, self.header.ApprovalStatus
        ]
        if 'RTPlanName' in self.header:
            vals.append(self.header.RTPlanName)
        else:
            vals.append(np.nan)
        if self.ud != 0:
            vals.append(self.ud)
        else:
            vals.append('%i or not defined' % 0)
        if 'SoftwareVersions' in self.header:
            vals.append(self.header.SoftwareVersions)
        else:
            vals.append(np.nan)
        vals.append(self.version)

        infoDf = pd.DataFrame(data=[valTypes, vals]).T

        # Now write info to 'Main' sheet, and leaf positions to
        # 'LeafJawPositions' sheet
        infoDf.to_excel(writer, sheet_name='Main', header=False, index=False)
        self.oldljp.to_excel(
            writer, sheet_name='LeafJawPositions', index=False)

        worksheet = writer.sheets['Main']
        worksheet.set_column('A:A', 25)
        worksheet.set_column('B:B', 90)

        # Save the spreadsheet
        writer.save()
        msg = 'The MLC values were saved in %s' % out_file
        infoNotif(self, 'Success!', msg)
        logging.info('Spreadsheet: %s', out_file)

    def getGeom(self):
        """Get the collimator geometry values."""
        if self.ui.radioButton_geom28.isChecked():
            self.geomX = 140
            self.geomY = 140
            self.ui.lineEdit_geomCustom.setEnabled(False)
        elif self.ui.radioButton_geom60.isChecked():
            self.geomX = 300
            self.geomY = 300
            self.ui.lineEdit_geomCustom.setEnabled(False)
        else:
            self.ui.lineEdit_geomCustom.setEnabled(True)
            try:
                self.geomX = self.ui.lineEdit_geomCustom.text().split(',')
                # We can only use squares as collimator edges
                self.geomX = float(self.geomX[0])
                self.geomY = float(self.geomX)
            # This occurs on first press of the custom radio button
            except (TypeError, ValueError, IndexError):
                pass

    def moveMLC(self):
        """Call the appropriate function to add motion to the MLCs."""
        self.layerNum()
        self.getGeom()
        # User config logging - random motion, don't move conebeam, etc??
        logging.info('Col geom %r', [self.geomX, self.geomY])
        logging.info('Imaging fields ignored %s',
                     self.ui.checkBox_imgFields.isChecked())
        logging.info('Imaging fields count %s',
                     self.ui.lineEdit_imgFieldCount.text())
        logging.info('Layers %i', self.layers)
        logging.info('Preserve Layers %s',
                     self.ui.checkBox_preserveLayers.isChecked())
        logging.info('Keep static %s',
                     self.ui.checkBox_staticLeaves.isChecked())
        logging.info('Randomize %s', self.ui.radioButton_Rand.isChecked())
        logging.info('Simulation %s', self.ui.checkBox_dryRun.isChecked())

        if self.ui.radioButton_VMAT.isChecked():
            logging.info('VMAT')
            self.moveMlcVmat()
        elif self.ui.radioButton_IMRT.isChecked():
            self.moveMlcImrt()
            logging.info('IMRT')

    def moveMlcVmat(self):
        """
        Add/subtract a value to each MLC position, within a uniform
        distribution. For VMAT plans.
        """
        # Don't enable the new RP generation button until everything is ready
        if self.enableGo() == 1:
            return

        # Empty in case of previous runs
        self.tmpljp = []
        self.newljp = []

        # Get motion to add to each leaf
        self.ud = float(self.ui.lineEdit_uniformDist.text())
        print('Uniform distribution is within ±', self.ud, 'mm')
        logging.info('Uniform dist: %f', self.ud)

        # Keep the original leaf positions intact
        self.tmpljp = deepcopy(self.oldljp)

        # Identify the imaging sequences (i.e. conebeam)
        if self.ui.checkBox_imgFields.isChecked():
            imgFields = int(self.ui.lineEdit_imgFieldCount.text())
            self.planInfo['conebeam'] = self.tmpljp.columns.values[0:imgFields]
            conebeam = self.tmpljp.loc[:, self.planInfo['conebeam']]

            imgFieldCount = len(self.getNumOfBeams(self.planInfo['conebeam']))

            # Redefine tmpljp, angles, and collimator boundaries to not include
            # cone beam sequences.
            self.tmpljp = self.tmpljp.iloc[:, imgFields:]
            colAngles = self.colAngles[imgFieldCount:]
            leafBoundaries = self.leafBoundaries[imgFieldCount:]
        else:
            conebeam = []

        # Get a nested list of all headers, grouped by arc. It's important that
        # this comes after the imaging portion of the plan is removed (if
        # applicable).
        self.planInfo['arcList'] = self.getNumOfBeams(self.tmpljp.columns)

        if self.ui.radioButton_twoLayers.isChecked():
            self.planInfo['upperLayer'] = self.tmpljp.columns.values[0::2]
            self.planInfo['lowerLayer'] = self.tmpljp.columns.values[1::2]

            upperLayer = self.tmpljp.loc[:, self.planInfo['upperLayer']]
            lowerLayer = self.tmpljp.loc[:, self.planInfo['lowerLayer']]

            upperBak = deepcopy(upperLayer)
            lowerBak = deepcopy(lowerLayer)

        # Setup x1Bounds and x2Bounds to get boundary positions for all leaves.
        x1Bounds, x2Bounds = self.getColBoundaries(colAngles, leafBoundaries)

        # Since lowers depend on upper leaves, change upper boundaries
        x1Bounds, x2Bounds = utils.restrictedColBounds(x1Bounds, x2Bounds)

        # Add random values (with a specified uniform distribution) to the
        # original MLC leaf jaw positions.
        if self.ui.radioButton_Rand.isChecked():

            # Only add random motion to the upper MLC layer, then make sure
            # that the lower leaves do not shape the beam by pulling them
            # 0.1mm farther from center than the overhead upper leaves.
            if self.ui.checkBox_preserveLayers.isChecked():

                # Add the random data
                upperLayer = self.randMlcVmat(upperLayer)

            # # Add random motion to both MLC layers - not very much tested.
            # # Almost certainly will break somewhere.
            # elif not self.ui.checkBox_preserveLayers.isChecked():
            #     self.newljp = self.randMlcVmat(self.tmpljp)
            #     self.newljp = self.fixOverlap(self.newljp)

        # Add systematic error (identical motion) to all MLC leaves.
        else:
            if self.ui.checkBox_staticLeaves.isChecked():
                upperArcList = self.getNumOfBeams(self.planInfo['upperLayer'])
                for headerSeq in upperArcList:
                    # Find all provided leaf positions within an arc
                    arcSeqs = upperLayer.loc[:, headerSeq]
                    origArcSeqs = self.oldljp.loc[:, headerSeq]

                    if self.ui.checkBox_staticLeaves.isChecked():
                        arcSeqs = dep.keepStatics(self, arcSeqs)
                        arcSeqs = dep.staticLeafPairs(self, arcSeqs,
                                                      origArcSeqs)
                    upperLayer.loc[:, headerSeq] = arcSeqs

            upperLayer = dep.systematic(self, upperLayer)

        # Upper leaf positions shouldn't include dynamic leaf gap errors
        upperLayer = dc.dynamicLeafGapCheck(self, upperLayer)
        # Fix upper leaf collisions between sides X1 and X2
        upperLayer = dc.fixCollision(self, upperLayer)

        # Fix upper leaves that are out-of-bounds, without introducing
        # new collisions or DLG errors
        for i, arcHeaders in enumerate(self.planInfo['arcList']):
            # Limit upper headers to those within a particular arc
            upperArcHeaders = [
                x for x in self.planInfo['upperLayer'] if x in arcHeaders
            ]
            upperX1Bounds = x1Bounds[i][0]
            upperX2Bounds = x2Bounds[i][0]

            arcUppers = upperLayer.loc[:, upperArcHeaders]

            arcUppers = dc.upperOutOfBounds(self, arcUppers, upperX1Bounds,
                                            upperX2Bounds)
            upperLayer.loc[:, upperArcHeaders] = arcUppers

        # Fix lower leaves extending into beam.
        lowerLayer = dc.lowerShapeBeam(self, upperLayer, lowerLayer)

        # Replace static leaf positions in the upper layer with original
        # positions
        upperLayer.fillna(value=upperBak, inplace=True)

        # # Fix lower leaves that are outside the collimator boundaries.
        # # As collimator boundaries change across arcs, this must be run
        # # once per arc
        # for i, arcHeaders in enumerate(self.planInfo['arcList']):
        #     # It might be more efficient to use list(set().intersection)
        #     # here but it returns header lists out of order
        #     upperHeaders = [x for x in self.planInfo['upperLayer'] if
        #                      x in arcHeaders]
        #     lowerHeaders = [x for x in self.planInfo['lowerLayer'] if
        #                        x in arcHeaders]
        #
        #     # Lower boundaries only
        #     lowerX1Bounds = x1Bounds[i][1]
        #     lowerX2Bounds = x2Bounds[i][1]
        #
        #     arcUppers = upperLayer.loc[:, upperHeaders]
        #     arcLowers = lowerLayer.loc[:, lowerHeaders]
        #
        #     arcUppers, arcLowers = dc.lowerOutOfBounds(self,
        #                                                   arcUppers,
        #                                                   arcLowers,
        #                                                   lowerX1Bounds,
        #                                                   lowerX2Bounds)
        #
        #     upperLayer.loc[:, upperHeaders] = arcUppers
        #     lowerLayer.loc[:, lowerHeaders] = arcLowers
        #
        # # Fix lower leaves that extend into field due to domino
        # # effect
        # lowerLayer = dc.lowerShapeBeam(self, upperLayer,
        #                                  lowerLayer)

        # Recombine upper and lower layers
        self.tmpljp.loc[:, self.planInfo['upperLayer']] = upperLayer
        self.tmpljp.loc[:, self.planInfo['lowerLayer']] = lowerLayer

        # Re-add the conebeam or orthogonal imaging sequences if needed
        if self.ui.checkBox_imgFields.isChecked():
            self.tmpljp = pd.concat([conebeam, self.tmpljp], axis=1)

        # Correct all positions to 0.01mm and make sure all values are floats
        self.newljp = self.tmpljp.round(2)
        self.newljp.apply(pd.to_numeric, errors='ignore')

        # Get total random shift data
        normUpperMean, normUpperStdev, upperMean, upperStdev = self.mlcStats(
            upperBak, upperLayer)
        normLowerMean, normLowerStdev, lowerMean, lowerStdev = self.mlcStats(
            lowerBak, lowerLayer)
        normLeafMean, normStdev, leafMean, stdev = self.mlcStats(
            self.oldljp, self.newljp)

        # Keep for later use
        self.upperStatVals = [normUpperMean, upperMean, normUpperStdev, upperStdev]
        self.lowerStatVals = [normLowerMean, lowerMean, normLowerStdev, lowerStdev]
        self.statVals = [normLeafMean, leafMean, normStdev, stdev]

        logging.info('Total norm avg shift: %f, Total avg shift %f' %
                     (normLeafMean, leafMean))
        logging.info(
            'Total norm stdev %f, Total sdev: %f' % (normStdev, stdev))

        print('##########################################################\n'
              'Total normalized average shift: %f mm\n'
              'Total average shift: %f mm\n'
              '\n'
              'Total normalized stdev: %f mm\n'
              'Total  stdev: %f mm\n' % (normLeafMean, leafMean, normStdev,
                                         stdev))

        if self.mlcType == 'twoLayerA':
            logging.info('Norm upper shift %f, avg upper shift: %f' %
                         (normUpperMean, upperMean))
            logging.info('Norm upper stdev %f, avg upper sdev: %f' %
                         (normUpperStdev, upperStdev))
            logging.info('Norm lower shift %f, avg lower shift: %f' %
                         (normLowerMean, lowerMean))
            logging.info('Norm lower stdev %f, lower sdev: %f' %
                         (normLowerStdev, lowerStdev))

            print('Upper normalized average shift: %f mm\n'
                  'Upper average shift: %f mm\n'
                  '\n'
                  'Upper normalized stdev: %f mm\n'
                  'Upper stdev: %f mm\n'
                  '\n'
                  'Lower normalized average shift: %f mm\n'
                  'Lower average shift: %f mm\n'
                  '\n'
                  'Lower normalized stdev: %f mm\n'
                  'Lower stdev: %f mm\n' %
                  (normUpperMean, upperMean, normUpperStdev, upperStdev,
                   normLowerMean, lowerMean, normLowerStdev, lowerStdev))

        if not self.ui.checkBox_dryRun.isChecked():
            self.genNewRP()

    def getNumOfBeams(self, headerList):
        """
        Find all sequence headers within each arc
        :param headerList: a flat list of sequence headers. Also accepts 1D
        numpy arrays.
        :return allArcList: a nested list of sequence headers, grouped by arc.
        Each sub-list contains all the headers for that particular arc. Note
        that this depends on the provided headerList - if certain arcs are
        excluded from headerList, then there won't be a corresponding sublist
        in allArcList.
        """

        arcs = []  # Temporary storage list for arcs
        for header in headerList:
            # Arc number is the first value in a sequence header
            arc = header.split(':')[0]
            if arc not in arcs:
                arcs.append(arc)

        # Use the arc number (1, 2, 3...) to find all sequence headers within
        # the arc
        allArcList = []
        for arc in arcs:
            singleArcList = []
            for header in headerList:
                if header.startswith(str(arc)):
                    singleArcList.append(header)
            allArcList.append(singleArcList)

        return allArcList

    def randMlcVmat(self, tmpljp):
        """
        Add random motion to the provided sequence of Leaf Jaw Positions and
        return the new values for later use.
        :param tmpljp: A pandas DataFrame of leaf positions
        :return tmpljp: The same DataFrame with random values added. If
        self.keepStatics() is called, some values may have been replaced by
        np.nan
        """
        upperArcList = self.getNumOfBeams(self.planInfo['upperLayer'])

        for headerSeq in upperArcList:
            # Find all provided leaf positions within an arc
            arcSeqs = tmpljp.loc[:, headerSeq]
            origArcSeqs = self.oldljp.loc[:, headerSeq]

            if self.ui.checkBox_staticLeaves.isChecked():
                arcSeqs = dep.keepStatics(self, arcSeqs)
                arcSeqs = dep.staticLeafPairs(self, arcSeqs, origArcSeqs)

            # Now add random values to leaf positions
            arcSeqs = dep.rand(self, arcSeqs)

            tmpljp.loc[:, headerSeq] = arcSeqs

        return tmpljp

    def getColBoundaries(self, angleset, leafBoundaries):
        """
        Set up a list of the maximum positions (in the horizontal direction)
        that all leaves must be within.
        x1Bounds and x2Bounds are a little confusing initially.
        They're laid out like x1Bounds[arc][layer][leaf]
        """
        # Set up the original collimator boundaries.
        self.originalCol = box(-self.geomX, -self.geomX, self.geomX,
                               self.geomX)

        tmpMask = []
        for i, angle in enumerate(angleset):
            newCol = affinity.rotate(self.originalCol, -angle, origin=(0, 0))
            inner = self.originalCol.intersection(newCol)
            inner = LineString(inner.exterior.coords)

            arcBounds = []  # All boundaries for an arc
            for j in range(len(leafBoundaries[i])):
                layerBounds = []
                for yVal in leafBoundaries[i][j]:
                    lim = LineString([(-500, yVal), (500, yVal)])
                    int1 = lim.intersection(inner)
                    # There is no intersection on the first and last leaves
                    # on Halcyon's lower layer as they are 5mm lower/higher
                    # than the collimator edge
                    multiset = int1.wkt.split(' ')[1:]
                    # We have to be creative here since the intersection may
                    # be a MultipointGeometry (shapely object) which isn't
                    # as nice to deal with as a LineString
                    for k, val in enumerate(multiset):
                        if val == 'EMPTY':
                            multiset[k] = False
                        elif val[0] == '(':
                            multiset[k] = float(val[1:])
                        elif val[-1] == ',':
                            multiset[k] = float(val[:-1])
                        elif val[-1] == ')':
                            multiset[k] = float(val[:-1])
                        else:
                            multiset[k] = float(val)
                    if type(multiset[0]) is bool:
                        layerBounds.append(False)
                    # Only get the x values
                    else:
                        layerBounds.append([multiset[0], multiset[2]])
                arcBounds.append(layerBounds)
            tmpMask.append(arcBounds)

        # Get the actual limits by finding the intersection point (of the two
        # per leaf) that is closes to center.
        x1Bounds = []
        x2Bounds = []
        for i, arc in enumerate(tmpMask):
            arcMaskX1 = []
            arcMaskX2 = []
            for j, layer in enumerate(arc):
                layerMaskX1 = []
                layerMaskX2 = []
                for k in range(len(layer) - 1):
                    # First leaf, lower layer
                    if layer[k] is False:
                        layerMaskX1.append(tmpMask[i][0][0][0])
                        layerMaskX2.append(tmpMask[i][0][0][1])
                        # layerMaskX1.append(layer[k + 1][0])
                        # layerMaskX2.append(layer[k + 1][1])
                    # Last leaf, lower layer
                    elif layer[k + 1] is False:
                        layerMaskX1.append(tmpMask[i][0][-1][0])
                        layerMaskX2.append(tmpMask[i][0][-1][1])

                        # layerMaskX1.append(layer[k][0])
                        # layerMaskX2.append(layer[k][1])

                    else:
                        # Intersections on the left side.
                        if abs(layer[k][0]) < abs(layer[k + 1][0]):
                            minBound = layer[k][0]
                        else:
                            minBound = layer[k + 1][0]

                        # Intersections on the right side
                        if abs(layer[k][1]) < abs(layer[k + 1][1]):
                            maxBound = layer[k][1]
                        else:
                            maxBound = layer[k + 1][1]

                        layerMaskX1.append(minBound)
                        layerMaskX2.append(maxBound)

                arcMaskX1.append(layerMaskX1)
                arcMaskX2.append(layerMaskX2)
            x1Bounds.append(arcMaskX1)
            x2Bounds.append(arcMaskX2)
        return x1Bounds, x2Bounds

    def externPlan(self):
        """
        Generate a new RP*.dcm using LeafJawPositions previously
        defined/modified in an .xlsx file.
        This does no error/overlap testing.
        """
        # Reset in case of previous runs
        self.newljp = []
        # Make sure the "parent" RP dicom is defined.
        if self.filename == '':
            self.getDicom()
        # Get the spreadsheet
        self.externFile = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Select new leaf positions spreadsheet')
        self.externFile = str(self.externFile[0])
        print('External Plan:', self.externFile)

        data = pd.read_excel(self.externFile, sheet_name='LeafJawPositions')
        self.newljp = data
        self.genNewRP()

    def moveMlcImrt(self):
        """
        Add/subtract a random value to each MLC position, within a uniform
        distribution. For IMRT plans.
        """
        msg = 'IMRT plan modification is not currently supported.'
        infoNotif(self, 'Not supported', msg)
        self.ui.radioButton_VMAT.setChecked(True)

    def mlcStats(self, oldljp, newljp):
        """
        Calculate the average shift and standard deviation of the new
        ljp sequence.
        """
        # Get the difference between each leaf's original and final positions:
        diff = newljp - oldljp

        # We need a "flat" list for accurate mean and standard deviation
        # calculations.
        diffFlat = diff.values.flatten()

        # Remove zeros (times when the leaf is not moved) for normalized values.
        normDiffFlat = diffFlat[diffFlat != 0]

        # If there are no non-zero values in the normalized array, set
        # mean and stdev as 0 instead of nan
        if normDiffFlat.size == 0:
            normMean = 0
            normStdev = 0
        else:
            normMean = np.nanmean(normDiffFlat)
            normStdev = np.nanstd(normDiffFlat)

        leafMean = np.nanmean(diffFlat)
        stdev = np.nanstd(diffFlat)

        return normMean, normStdev, leafMean, stdev

    def genNewRP(self):
        """
        This does the actual work of generating a new RP dicom using data
        from either one of the moveMLC functions or the externPlan function.
        """
        newHeader = deepcopy(self.header)
        for position in self.newljp.columns:
            i, j, k = position.split(':')
            i = int(i)
            j = int(j)
            k = int(k)
            newPositions = self.newljp.loc[:, position]
            # Strip any remaining np.nans
            newPositions = newPositions[~np.isnan(newPositions)]
            newHeader.BeamSequence[i].ControlPointSequence[
                j].BeamLimitingDevicePositionSequence[
                    k].LeafJawPositions = newPositions

        if len(self.ui.lineEdit_rtPlanLabel.text()) > 16:
            msg = (
                'The length of the RT Plan Label should not be more than 16 '
                'characters long.')
            errorNotif(self, msg)

        # Get a count of how many beams were modified. It should be safe to
        # assume that each imaging sequence (or each pair if two layers) are
        # in their own arc
        activeBeams = self.beams
        if self.ui.checkBox_imgFields.isChecked():
            if self.ui.radioButton_twoLayers.isChecked():
                numImgBeams = int(self.ui.lineEdit_imgFieldCount.text()) / 2
            else:
                numImgBeams = int(self.ui.lineEdit_imgFieldCount.text())
            activeBeams = int(self.beams - numImgBeams)

        # Set the output RP dicom name from active beam count and type of error
        if self.ui.radioButton_Rand.isChecked():
            newRpName = str(activeBeams) + 'fld_' + str(int(
                self.ud)) + 'rand' + '.dcm'
        else:
            newRpName = str(activeBeams) + 'fld_' + str(int(
                self.ud)) + 'sys' + '.dcm'

        # Set the new RT Plan Label - probably will want this different from
        # original.
        newHeader.RTPlanLabel = self.ui.lineEdit_rtPlanLabel.text()

        # Offer to change the UID - this can be good especially when dealing
        # with TPS like Eclipse. If yes, replace the last part of the old UID
        # with the current date and time.
        msg = 'Change the RP UID?'
        if askQuestion(self, 'Change UID', msg) == 'yes':
            today_str = datetime.now().strftime('%Y%m%d%H%M%S')
            sopUid = self.header.SOPInstanceUID.split('.')
            # Remove the original date and time (the last part of the UID)
            sopUid.pop()
            # Keep the standard UID element separation with '.'
            sopUid = '.'.join(sopUid)
            newHeader.SOPInstanceUID = sopUid + '.' + today_str

        # Save it!
        out_path = os.path.join(self.pwd, 'Output', newRpName)
        newHeader.save_as(out_path)
        # Test for correct write.
        if pydicom.misc.is_dicom(out_path):
            msg = 'The new DICOM plan was saved in %s' % os.path.join(
                self.pwd, 'Output', newRpName)
            infoNotif(self, 'Saved', msg)

            logging.info('New UID: %s', newHeader.SOPInstanceUID)
            logging.info('New RP*.dcm %s', out_path)

        # Now write some info to a csv file:
        if self.ui.checkBox_csvOut.isChecked():
            header = [
                'Patient Name', 'Edited Arcs/Fields', 'Imaging Sequences',
                'Original File', 'Original Plan', 'New File', 'New Plan',
                'Value', 'Type', 'Norm Avg Shift', 'Avg Shift', 'Norm Stdev',
                'Stdev', 'Norm Upper Shift', 'Upper Shift', 'Norm Upper Stdev',
                'Upper Stdev', 'Norm Lower Shift', 'Lower Shift',
                'Norm Lower Stdev', 'Lower Stdev'
            ]

            imSeqCount = 0
            if self.ui.checkBox_imgFields.isChecked():
                imSeqCount = self.planInfo['conebeam'].size

            errType = 'S'
            if self.ui.radioButton_Rand.isChecked():
                errType = 'R'

            stats = [
                str(self.header.PatientName), activeBeams, imSeqCount,
                self.filename.split('/')[-1], self.header.RTPlanLabel,
                newRpName, newHeader.RTPlanLabel, self.ud, errType
            ]

            stats = stats + self.statVals + self.upperStatVals + \
            self.lowerStatVals

            resultsFile = os.path.join(self.pwd, 'Stats.csv')
            if os.path.isfile(resultsFile):
                with open(resultsFile, 'a', newline='') as csvfile:
                    book = csv.writer(csvfile, delimiter=',', dialect='excel')
                    book.writerow(stats)
            else:
                with open(resultsFile, 'w', newline='') as csvfile:
                    book = csv.writer(csvfile, delimiter=',', dialect='excel')
                    book.writerow(header)
                    book.writerow(stats)

        # Clear temporary values - just to be safe! Only runs when a dicom is
        # actually written, may want to change later to run always.
        self.clearTmp()


def main():
    """
    Call Gui() class to construct UI and handle setting icons and initial title.
    """
    if system() == 'Windows':  # Display the icon in the taskbar
        if 'Vista' not in platform():
            myappid = 'MLC Edit'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                myappid)
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    ex1 = Gui()
    ex1.setWindowTitle('MLC Edit')
    ex1.setWindowIcon(QtGui.QIcon('hospital.png'))
    ex1.show()
    ex1.activateWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
