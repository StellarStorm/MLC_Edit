"""
Do a whole bunch of corrections! This is for plans with two-layer MLC design
(i.e. plans for Halcyon), after applying randomized or systematic errors.
Use this to fix:
(1). Dynamic leaf gap error:                    dynamicLeafGapCheck
(2). Leaf collisions:                           fixCollisions
(3). Upper leaves outside collimator edge:      upperOutOfBounds
(4). Lower leaves shape beam:                   lowerShapeBeam
(5). Lower leaves outside collimator edge:      lowerOutOfBounds

When "IndependentCorrections" class is added, it will be similar. However, since
it should be used for cases where the lower layer can move independently of
the upper layer, (4) won't be needed and there will be less upper vs. lower
checking.
"""

import numpy as np


class DependentCorrections:
    """
    This class corrects the new, potential leaf positions for machines that have
    both a upper (closer to source) and a lower (closer to target) MLC
    layer, but the lower layer is not allowed to extend into the field or
    shape the beam. Example - Varian's Halcyon with Eclipse 15.1.01 does not
    allow delivery of plans where lower leaves shape the beam.
    """

    def init(self, parent=None):
        """Do nothing for now."""
        pass

    def dynamicLeafGapCheck(self, upperLayer):
        """
        Enforce minimum separation of leaves. Leaves must either be completely
        closed or open past a certain amount. Unlike upperOutOfBounds(), this
        accepts multi-arc DataFrames easily.

        :param upperLayer: a pandas DataFrame of upper leaf positions.

        :return upperLayer: the same DataFrame with dynamic leaf-gap errors
        corrected
        """

        for sequence in upperLayer.columns:
            # Looping over a DataFrame is awfully slow, so we use the underlying
            # numpy array instead
            x1Seq = upperLayer.loc[self.planInfo['dX1'], sequence].values
            x2Seq = upperLayer.loc[self.planInfo['dX2'], sequence].values

            for i, leaf1 in enumerate(x1Seq):
                leaf2 = x2Seq[i]
                # Skip for dynamic leaf pair
                if not np.all(np.isnan([leaf1, leaf2])):
                    gap = abs(leaf2 - leaf1)
                    if 0 < gap < self.minSep:
                        moveEach = (self.minSep - gap) / 2
                        x1Seq[i] = leaf1 - moveEach
                        x2Seq[i] = x1Seq[i] + self.minSep

            upperLayer.loc[self.planInfo['dX1'], sequence] = x1Seq
            upperLayer.loc[self.planInfo['dX2'], sequence] = x2Seq
        return upperLayer

    def fixCollision(self, upperLayer):
        """
        Fix colliding leaves after applying random motion.
        Note that this WILL potentially cause leaves to move out-of-bounds if
        applied before upperOutOfBounds(). Because simply closing overlapping
        leaf pairs can cause Eclipse to report dynamic leaf gap errors, this
        moves them self.minSep apart.

        :param upperLayer: a pandas DataFrame of upper leaf positions

        :return upperLayer: the same DataFrame with any leaf collisions
        fixed
        """

        for sequence in upperLayer.columns:
            # Looping over a DataFrame is awfully slow, so we use the underlying
            # numpy array instead
            x1Seq = upperLayer.loc[self.planInfo['dX1'], sequence].values
            x2Seq = upperLayer.loc[self.planInfo['dX2'], sequence].values

            for i, leaf1 in enumerate(x1Seq):
                leaf2 = x2Seq[i]
                if leaf1 > leaf2:
                    overlap = abs(leaf2 - leaf1)
                    x1Seq[i] = leaf1 - ((overlap + self.minSep) / 2)
                    x2Seq[i] = x1Seq[i] + self.minSep
                    # print(sequence, i, overlap, self.minSep, [leaf1, leaf2],
                    #       [x1Seq[i], x2Seq[i]])

            upperLayer.loc[self.planInfo['dX1'], sequence] = x1Seq
            upperLayer.loc[self.planInfo['dX2'], sequence] = x2Seq
        return upperLayer

    def upperOutOfBounds(self, upperLayer, x1Bounds, x2Bounds):
        """
        Find any upper layer leaves that are outside the collimator boundary
        position, and reset them to the maximum (or minimum if on X1 side)
        possible position inside bounds. Then make sure that the new leaf
        position doesn't cause a collision or DLG error with the opposing leaf.
        Must be run on a per-arc basis.

        :param upperLayer: a pandas DataFrame of upper leaf positions for a
        single arc.
        :param x1Bounds: A list of minimum allowed positions within bounds for
        upper leaves on the X1 side of collimator. Also accepts numpy arrays.
        :param x2Bounds: A list of minimum allowed positions within bounds for
        upper leaves on the X2 side of collimator. Also accepts numpy arrays.

        :return upperLayer: the same pandas DataFrame of upper leaf positions,
        but corrected so that no upper leaves are out of bounds.
        """

        for sequence in upperLayer.columns:
            # Looping over a DataFrame is awfully slow, so we use the underlying
            # numpy array instead
            x1Seq = upperLayer.loc[self.planInfo['dX1'], sequence].values
            x2Seq = upperLayer.loc[self.planInfo['dX2'], sequence].values

            for i, leaf in enumerate(x1Seq):
                if leaf < x1Bounds[i]:
                    x1Seq[i] = x1Bounds[i]
                    if x1Seq[i] + self.minSep > x2Seq[i]:
                        x2Seq[i] = x1Seq[i] + self.minSep
                elif leaf > x2Bounds[i]:
                    x1Seq[i] = x2Bounds[i] - self.minSep
                    x2Seq[i] = x2Bounds[i]

            for i, leaf in enumerate(x2Seq):
                if leaf < x1Bounds[i]:
                    x2Seq[i] = x1Bounds[i] + self.minSep
                    x1Seq[i] = x1Bounds[i]
                elif leaf > x2Bounds[i]:
                    x2Seq[i] = x2Bounds[i]
                    if x2Seq[i] - self.minSep < x1Seq[i]:
                        x1Seq[i] = x2Seq[i] - self.minSep

            upperLayer.loc[self.planInfo['dX1'], sequence] = x1Seq
            upperLayer.loc[self.planInfo['dX2'], sequence] = x2Seq
        return upperLayer

    def lowerShapeBeam(self, upperLayer, lowerLayer):
        """
        Correct lower leaves that shape the beam (by extending into the
        field past the overhead upper leaf or leaves). This may return lower
        leaf positions that may be outside collimator boundaries.

        Note that lower leaves must not only not shape the beam, but also be
        at least 0.1mm behind overhead upper leaves.

        For consistency's sake, this currently moves all lower leaves to
        0.1mm behind their overhead upper leaves. While this maintains
        the same behavior as a previous version, this behavior will be changed
        at a later date to only move lower leaves that are actively shaping
        the beam.

        :param upperLayer: a pandas DataFrame of upper layer leaf positions.
        Will not be edited or returned.
        :param lowerLayer: a pandas DataFrame of lower layer leaf
        positions. Will be edited.

        :return lowerLayer: the same pandas DataFrame of lower layer leaf
        positions, corrected so that they no longer shape the beam.
        """

        for i, sequence in enumerate(lowerLayer.columns):
            # Using underlying numpy array to speed up looping
            lowerX1Seq = lowerLayer.loc[self.planInfo['pX1'], sequence].values
            lowerX2Seq = lowerLayer.loc[self.planInfo['pX2'], sequence].values
            # Have to use indexing here since the column header for upper layer
            # will be different for upper and lower layers
            # Also using underlying numpy array to speed up looping
            upperX1Seq = upperLayer.iloc[self.planInfo['dX1'], i].values
            upperX2Seq = upperLayer.iloc[self.planInfo['dX2'], i].values

            # Bank X1 leaves (left side for BEV)
            for j, _ in enumerate(lowerX1Seq):
                # First lower leaf so only one overhead upper leaf pair
                if j == 0:
                    upperLeafX1a = upperX1Seq[0]
                    # Only move lower leaf if overhead upper is not a
                    # static leaf
                    if not np.isnan(upperLeafX1a):
                        lowerX1Seq[j] = upperLeafX1a - 0.1

                # Last lower leaf - again, only one overhead upper leaf pair
                elif j == lowerX1Seq.size - 1:
                    upperLeafX1a = upperX1Seq[-1]
                    # Only move lower leaf if overhead upper is not a
                    # static leaf
                    if not np.isnan(upperLeafX1a):
                        lowerX1Seq[j] = upperLeafX1a - 0.1

                else:
                    upperLeafX1a = upperX1Seq[j]
                    upperLeafX1b = upperX1Seq[j - 1]
                    # Only move lower leaves if at least one of the overhead
                    # upper leaves are dynamic.
                    if not np.all(np.isnan([upperLeafX1a, upperLeafX1b])):
                        lowerX1Seq[j] = np.nanmin([upperLeafX1a, upperLeafX1b
                                                   ]) - 0.1

            # Bank X2 leaves (right side for BEV)
            for j, _ in enumerate(lowerX2Seq):
                # First lower leaf so only one overhead upper leaf pair
                if j == 0:
                    upperLeafX2a = upperX2Seq[0]
                    # Only move lower leaf if overhead upper is not a
                    # static leaf
                    if not np.isnan(upperLeafX2a):
                        lowerX2Seq[j] = upperLeafX2a + 0.1

                # Last lower leaf - again, only one overhead upper leaf pair
                elif j == lowerX2Seq.size - 1:
                    upperLeafX2a = upperX2Seq[-1]
                    # Only move lower leaf if overhead upper is not a
                    # static leaf
                    if not np.isnan(upperLeafX2a):
                        lowerX2Seq[j] = upperLeafX2a + 0.1

                else:
                    upperLeafX2a = upperX2Seq[j]
                    upperLeafX2b = upperX2Seq[j - 1]
                    # Only move lower leaves if at least one of the overhead
                    # upper leaves are dynamic.
                    if not np.all(np.isnan([upperLeafX2a, upperLeafX2b])):
                        lowerX2Seq[j] = np.nanmax([upperLeafX2a, upperLeafX2b
                                                   ]) + 0.1

            lowerLayer.loc[self.planInfo['pX1'], sequence] = lowerX1Seq
            lowerLayer.loc[self.planInfo['pX2'], sequence] = lowerX2Seq

        return lowerLayer
