"""
Various utilities
"""


def restrictedColBounds(x1Bounds, x2Bounds):
    """
    Use when lower leaves are not allowed to shape the beam (i.e. Halcyon
    with earlier implementations of Eclilpse). This recognizes that:
        (a). Although there are upper boundaries, the positions of upper
            boundaries are ultimately dependent on the lower boundaries.
        (b). The "real" upper boundary for any leaf is the maximum (if on
            the X1 side) + 0.1mm,  or minimum (if on the X2 side) - 0.1mm of
            the two underlying lower leaf boundaries.
    :params x1Bounds: a nested list of upper and lower leaf boundaries on
    the X1 (left) side of the collimator, indexed by arc, layer, and then leaf.
    :params x2Bounds: a nested list of upper and lower leaf boundaries on
    the X2 (right) side of the collimator, indexed by arc, layer, and then leaf.

    :returns x1Bounds: a similar nested list of X1 Boundaries, but with each
    upper boundary replaced by the maximum of the underlying lower leaf
    boundaries + 0.1.
    :returns x2Bounds: a similar nested list of X1 Boundaries, but with each
    upper boundary replaced by the minimum of the underlying lower leaf
    boundaries - 0.1.
    """
    for i, arcBounds in enumerate(x1Bounds):
        lowerLayer = arcBounds[1]

        upperLayer = []
        for j, leaf in enumerate(lowerLayer):
            # Upper layer should have one less boundary than lower layer
            if j < len(lowerLayer) - 1:
                leaf2 = lowerLayer[j + 1]
                upperLayer.append(max(leaf, leaf2) + 0.1)
        x1Bounds[i][0] = upperLayer

    for i, arcBounds in enumerate(x2Bounds):
        lowerLayer = arcBounds[1]

        upperLayer = []
        for j, leaf in enumerate(lowerLayer):
            # Upper layer should have one less boundary than lower layer
            if j < len(lowerLayer) - 1:
                leaf2 = lowerLayer[j + 1]
                upperLayer.append(min(leaf, leaf2) - 0.1)
        x2Bounds[i][0] = upperLayer

    return x1Bounds, x2Bounds
