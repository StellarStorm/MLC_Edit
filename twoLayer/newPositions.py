"""
This adds random or systematic errors to MLC leaf positions. Pre-existing
positions, collimator edge boundaries, and random or systematic error to be
added must already be defined and passed into the corresponding functions here.
The "Dependent" and "Independent" classes both add random or systematic error
for plans with two-layer MLCs, but differ based on if the lower layer can move
independently of the upper layer or not.
"""

import numpy as np


class Dependent:
    """
    This class is for machines that have both a upper (closer to source) and a
    lower (closer to target) MLC layer, but the lower layer is not allowed
    to extend into the field or shape the beam. Example - Varian's Halcyon with
    Eclipse 15.1.01 does not allow delivery of plans where lower leaves shape
    the beam.
    """

    def __init__(self, parent=None):
        pass

    def rand(self, upperLayer):
        """
        Add random motion to MLC leaf jaw positions - upper layer only.
        """
        # Even though for Halcyon the bottom two values for upperLayer
        # dataframe are NaN, it's safe to use upperLayer.shape here since any
        # value + NaN is still NaN
        randValues = np.random.uniform(-self.ud, self.ud, upperLayer.shape)
        newLayer = upperLayer + randValues
        return newLayer

    def systematic(self, upperLayer):
        """
        Add systematic error to MLC leaf jaw positions - i.e. all leaves are
        moved in the same direction by the same value. The value should be
        added to both upper and lower layers.
        """
        newLayer = upperLayer + self.ud
        return newLayer

    def keepStatics(self, tmpseq):
        """
        Find and replace all static leaves (leaves that don't move) in a beam
        sequence with np.nan. This is useful for later operations that add
        values to leaf positions, so that no new position is set for static
        leaves
        :param tmpseq: A pandas DataFrame of leaf positions. Must be on per-arc
        basis, that is, all leaf positions must be in the same arc.
        :return tmpseq: The same DataFrame, but with leaf positions that
        remain the same throughout an arc replaced by np.NaN
        """

        for i, leafSeries in enumerate(tmpseq.values):
            if len(np.unique(leafSeries)) == 1:
                tmpseq.iloc[i] = np.nan
        return tmpseq

    def staticLeafPairs(self, upperLayer, oldUpperLayer):
        """
        Sanity check to ensure that any leaves on the X1 side that are marked
        as static (by being replaced by np.nan) have a corresponding static
        leaf on the X2 side, and vice-versa.
        Must be run on a per-arc basis.

        :param upperLayer: pandas DataFrame of current upper leaf positions
        where any leaf positions that remain stationary throughout an arc were
        replaced by np.nan
        :param oldUpperLayer: pandas DataFrame of original upper leaf
        positions.

        :return upperLayer: pandas DataFrame of upper leaf positions with any
        leaf pairs for which both the X1 and X2 leaves are not stationary are
        replaced with their original values.
        """
        x1Seqs = upperLayer.loc[self.planInfo['dX1']]
        x2Seqs = upperLayer.loc[self.planInfo['dX2']]

        oldX1Seqs = oldUpperLayer.loc[self.planInfo['dX1']]
        oldX2Seqs = oldUpperLayer.loc[self.planInfo['dX2']]

        for sequence in upperLayer.columns:
            x1Seq = x1Seqs.loc[:, sequence].values
            x2Seq = x2Seqs.loc[:, sequence].values

            oldX1Seq = oldX1Seqs.loc[:, sequence].values
            oldX2Seq = oldX2Seqs.loc[:, sequence].values

            for i, leaf1 in enumerate(x1Seq):
                leaf2 = x2Seq[i]

                if np.isnan(leaf1) and np.isnan(leaf2):
                    pass
                elif np.isnan(leaf1):
                    x1Seq[i] = oldX1Seq[i]
                elif np.isnan(leaf2):
                    x2Seq[i] = oldX2Seq[i]

            upperLayer.loc[self.planInfo['dX1'], sequence] = x1Seq
            upperLayer.loc[self.planInfo['dX2'], sequence] = x2Seq
        return upperLayer


class Independent:
    """
    This class is for machines that have both a upper (closer to source) and a
    lower (closer to target) MLC layer, when both layers are allowed to move
    independently and shape the arc. Example - Varian's Halcyon with more
    recent versions of Eclipse.
    Currently this class is not used since I don't have a way to test the
    modified plans.
    """

    def __init__(self, parent=None):
        pass

    def rand(self, tmpljp):
        """
        (Eventually) Add random motion to all MLC leaves (random error). This
        function can skip over leaves that are stationary in the original plan.
        """
        if type(tmpljp) is not np.ndarray:
            tmpljp = np.ndarray(tmpljp)

    def systematic(self, tmpljp):
        """
        Add the same value to all MLC leaves (systematic error). This function
        can skip over leaves that are stationary in the original plan.
        """
